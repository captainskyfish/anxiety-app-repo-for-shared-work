﻿using Databox;
using System.Collections;
using UnityEngine;

public static class DataboxLib
{
    #region DatabaseStuff
    public static float GetFloatData(DataboxObject database, string tableID, string entryID, string valueID)
    {
        if (database.TryGetData(tableID, entryID, valueID, out FloatType _value))
        {
            return _value.Value;
        }
        else
        {
            return 0;
        }
    }
    public static int GetIntData(DataboxObject database, string tableID, string entryID, string valueID)
    {
        if (database.TryGetData(tableID, entryID, valueID, out IntType _value))
        {
            return _value.Value;
        }
        else
        {
            return 0;
        }
    }

    public static bool GetBoolData(DataboxObject database, string tableID, string entryID, string valueID)
    {
        if (database.TryGetData(tableID, entryID, valueID, out BoolType _value))
        {
            return _value.Value;
        }
        else
        {
            return false;
        }
    }

    public static string GetStringData(DataboxObject database, string tableID, string entryID, string valueID)
    {
        if (database.TryGetData(tableID, entryID, valueID, out StringType _StringType))
        {
            return _StringType.Value;
        }
        else
        {
            return null;
        }
    }

    public static Sprite GetSpriteData(DataboxObject database, string tableID, string entryID, string valueID)
    {
        if (database.TryGetData(tableID, entryID, valueID, out ResourceType _rscType))
        {
            return Resources.Load<Sprite>(_rscType.resourcePath);
        }
        else
        {
            return null;
        }
    }
    public static GameObject GetPrefabData(DataboxObject database, string tableID, string entryID, string valueID)
    {
        if (database.TryGetData(tableID, entryID, valueID, out ResourceType _rscType))
        {
            return Resources.Load<GameObject>(_rscType.resourcePath);
        }
        else
        {
            return null;
        }
    }
    #endregion DatabaseStuff
}