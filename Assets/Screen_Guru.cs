using Databox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class Screen_Guru : ScreenLogic
{
    const string GuruTableName = "GuruLessons";
    const string LevelRequirementKey = "LevelRequirement";
    const string LessonTextKey = "LessonText";
    const string LessonNameKey = "LessonName";

    public static Action<bool> OnGuruLessonStateChange;

    public LevelData currentLevel;
    public DailySaveDataManager levelSaveDataManager;
    //[!!!]CHANGE THIS LATER FOR SPRITES
    public Color[] coloursForButtonsCompletion;

    [Header("Guru Page Specifics")]
    LessonSelectionLogic lessonSelection;
    public DataboxObject guruDatabase;
    public List<Lesson> allLessons = new List<Lesson>();
    public GameObject backButton;
    public LessonSelectionLogic lessonSelectionLogic;

    private void OnEnable()
    {
        OnGuruLessonStateChange += ToggleStateChange;
    }

    private void OnDisable()
    {
        OnGuruLessonStateChange -= ToggleStateChange;
    }

    public void ToggleStateChange(bool isLessonActive)
    {
        backButton.SetActive(!isLessonActive);
    }

    public override void Setup()
    {
        LoadDatabase();
        levelSaveDataManager = FindObjectOfType<DailySaveDataManager>();
        backButton.SetActive(true);
        if (!guruDatabase.databaseLoaded)
        {
        LoadDatabase();
        }
        else
        {
        SetupAllLessons();
        }

        currentLevel = levelSaveDataManager.GetLevelDataByIndex(SaveData.PlayerLevel);

        lessonSelection = FindObjectOfType<LessonSelectionLogic>();
        lessonSelection.Setup(allLessons, this);

    }

    public void LoadDatabase()
    {
        guruDatabase.LoadDatabase();
    }

    public void SetupAllLessons()
    {
        var data = guruDatabase.GetEntriesFromTable(GuruTableName);

        foreach (var entry in data.Keys.Reverse())
        {
            int levelReq = 0;
            string lessonText = string.Empty;
            string lessonHeader = string.Empty;

            foreach (var item in guruDatabase.GetValuesFromEntry(GuruTableName, entry))
            {
                levelReq = GetIntData(guruDatabase, GuruTableName, entry, LevelRequirementKey);
                lessonText = GetStringData(guruDatabase, GuruTableName, entry, LessonTextKey);
                lessonHeader = GetStringData(guruDatabase, GuruTableName, entry, LessonNameKey);
            }

            Lesson lesson = new Lesson(levelReq, lessonText, lessonHeader);
            allLessons.Add(lesson);
        }

    }


    #region DatabaseStuff
    public static int GetIntData(DataboxObject database, string tableID, string entryID, string valueID)
    {
        IntType _value;
        if (database.TryGetData<IntType>(tableID, entryID, valueID, out _value))
        {
            return _value.Value;
        }
        else
        {
            return 0;
        }
    }

    private static string GetStringData(DataboxObject database, string tableID, string entryID, string valueID)
    {
        StringType _StringType;
        if (database.TryGetData<StringType>(tableID, entryID, valueID, out _StringType))
        {
            return _StringType.Value;
        }
        else
        {
            return null;
        }
    }
    #endregion DatabaseStuff

}

[System.Serializable]
public class Lesson
{
    public int levelRequirement;
    public string[] allLessonText;
    public string lessonName;
    public Lesson(int levelReq, string text,string lessonName = "No Name")
    {
        levelRequirement = levelReq;
        this.lessonName = lessonName;
        SetupText(text);
    }

    public void SetupText(string text)
    {
        string seperator = "[!]";
        allLessonText = text.Split(new string[] { seperator }, StringSplitOptions.None);
    }
}
