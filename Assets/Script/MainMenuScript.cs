using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class MainMenuScript : MonoBehaviour
{
    public static MainMenuScript instance;

    public readonly string dateLastPlayedKey = "date_last_played";
    public readonly string dailyBonusDayKey = "daily_bonus_day";
    public readonly string timerKey = "timer_day";

    public static int DailyTaskCompleteCount
    {
        get { return PlayerPrefs.GetInt(nameof(DailyTaskCompleteCount), 0); }
        set { PlayerPrefs.SetInt(nameof(DailyTaskCompleteCount), value); }
    }

    public GameObject dailyTaskPanel;

    public Action dailyTask;

    private void Awake()
    {
        instance = this;
    }

    float timer = 0;
    public void OnSetTimer()
    {
        var dateToDay = DateTime.Today;
        var dateLastPlayedStr = Convert.ToString(dateToDay, CultureInfo.InvariantCulture);
        PlayerPrefs.SetString(dateLastPlayedKey, dateLastPlayedStr);

        var dateLastPlayed = Convert.ToDateTime(dateLastPlayedStr, CultureInfo.InvariantCulture);
        var dateNow = DateTime.Now;
        var diff = dateNow.Subtract(dateLastPlayed);

        timer = (float)(diff.TotalHours + 11);
        PlayerPrefs.SetFloat(timerKey, timer);
    }

    public bool IsTimerEnded()
    {
        if (!PlayerPrefs.HasKey(dateLastPlayedKey))
        {
            return false;
        }

        var dateLastPlayedStr = PlayerPrefs.GetString(dateLastPlayedKey);
        var dateLastPlayed = Convert.ToDateTime(dateLastPlayedStr, CultureInfo.InvariantCulture);

        var dateNow = DateTime.Now;
        var diff = dateNow.Subtract(dateLastPlayed);

        timer = PlayerPrefs.GetFloat(timerKey);
        if (diff.TotalHours >= timer)
        {
            DailyTaskCompleteCount = 0;
            PlayerPrefs.DeleteKey(dateLastPlayedKey);
            PlayerPrefs.DeleteKey(dailyBonusDayKey);
            return true;
        }
        else
        {
            return false;
        }

    }

    public void EnableDailyTaskPanel()
    {
        dailyTaskPanel.SetActive(true);
    }
    public void DisableDailyTaskPanel()
    {
        dailyTaskPanel.SetActive(false);
    }
}
