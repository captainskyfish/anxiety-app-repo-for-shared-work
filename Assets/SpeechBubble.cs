﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SpeechBubble : MonoBehaviour
{
    public static event Action OnSpeechFinshed;
    public Utility_TextScrolling textScroller;

    [SerializeField] string[] allSpeechText;

    [SerializeField] int currentTextLineIndex;

    public Button nextTextButton;

    public bool inConversation;


    public void OnEnable()
    {

        ToggleNextButton(false);

        SetupNextTextButton();

        Utility_TextScrolling.OnLineFinished += Utility_TextScrolling_OnLineFinished;
        Utility_TextScrolling.OnLineStarted += Utility_TextScrolling_OnLineStarted;
    }
    private void OnDisable()
    {
        StopSpeech();
        Utility_TextScrolling.OnLineFinished -= Utility_TextScrolling_OnLineFinished;
        Utility_TextScrolling.OnLineStarted -= Utility_TextScrolling_OnLineStarted;
    }
    public void StartSpeech(string rawText)
    {
        inConversation = true;

        allSpeechText = SeperateRawText(rawText, "[!]");

        currentTextLineIndex = 0;

        SetupFirstLine();
    }

    public void StopSpeech()
    {
        FinishLesson();
        textScroller.ResetText();
    }

    public string[] SeperateRawText(string rawText, string seperator)
    {
        return rawText.Split(new string[] { seperator }, StringSplitOptions.None);
    }

    public void SetupFirstLine()
    {
        textScroller.SetupText(allSpeechText[0]);
    }

    public void ShowNextLine()
    {
        if (IsEndOfText())
            return;

        string lineOfText = allSpeechText[currentTextLineIndex];
        textScroller.SetupText(lineOfText);
    }

    public void SetupNextTextButton()
    {
        nextTextButton.onClick.AddListener(ShowNextLine);
    }

    public void ToggleNextButton(bool isActive)
    {
        nextTextButton.gameObject.SetActive(isActive);
    }

    public bool IsEndOfText()
    {
        return currentTextLineIndex >= allSpeechText.Length;
    }

    private void Utility_TextScrolling_OnLineFinished()
    {
        currentTextLineIndex++;

        if (IsEndOfText())
        {
            OnSpeechFinshed?.Invoke();
            Screen_Guru.OnGuruLessonStateChange?.Invoke(false);
            FinishLesson();

            return;
        }

        ToggleNextButton(true);
    }

    private void Utility_TextScrolling_OnLineStarted()
    {
        ToggleNextButton(false);
    }

    public void FinishLesson()
    {
        inConversation = false;
    }
}