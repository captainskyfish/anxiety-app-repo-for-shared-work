using System;
using UnityEngine;

namespace ES3Types
{
	[UnityEngine.Scripting.Preserve]
	[ES3PropertiesAttribute("levelNumber", "requireGuru", "requireDailyTasks", "requireBattle", "hasCompletedGuru", "hasCompletedDailyTasks", "hasCompletedBattle", "allDailyTasksForLevel")]
	public class ES3UserType_LevelData : ES3ScriptableObjectType
	{
		public static ES3Type Instance = null;

		public ES3UserType_LevelData() : base(typeof(LevelData)){ Instance = this; priority = 1; }


		protected override void WriteScriptableObject(object obj, ES3Writer writer)
		{
			var instance = (LevelData)obj;
			
			writer.WriteProperty("levelNumber", instance.levelNumber, ES3Type_int.Instance);
			writer.WriteProperty("requireGuru", instance.requireGuru, ES3Type_bool.Instance);
			writer.WriteProperty("requireDailyTasks", instance.requireDailyTasks, ES3Type_bool.Instance);
			writer.WriteProperty("requireBattle", instance.requireBattle, ES3Type_bool.Instance);
            writer.WriteProperty("hasCompletedGuru", instance.hasCompletedGuru, ES3Type_bool.Instance);
            writer.WriteProperty("hasCompletedConversation", instance.hasCompletedConversation, ES3Type_bool.Instance);
            writer.WriteProperty("hasCompletedDailyTasks", instance.hasCompletedDailyTasks, ES3Type_bool.Instance);
			writer.WriteProperty("hasCompletedBattle", instance.hasCompletedBattle, ES3Type_bool.Instance);
			writer.WriteProperty("allDailyTasksForLevel", instance.allDailyTasksForLevel);
		}

		protected override void ReadScriptableObject<T>(ES3Reader reader, object obj)
		{
			var instance = (LevelData)obj;
			foreach(string propertyName in reader.Properties)
			{
				switch(propertyName)
				{
					
					case "levelNumber":
						instance.levelNumber = reader.Read<System.Int32>(ES3Type_int.Instance);
						break;
					case "requireGuru":
						instance.requireGuru = reader.Read<System.Boolean>(ES3Type_bool.Instance);
						break;
					case "requireDailyTasks":
						instance.requireDailyTasks = reader.Read<System.Boolean>(ES3Type_bool.Instance);
						break;
					case "requireBattle":
						instance.requireBattle = reader.Read<System.Boolean>(ES3Type_bool.Instance);
						break;
					case "hasCompletedGuru":
						instance.hasCompletedGuru = reader.Read<System.Boolean>(ES3Type_bool.Instance);
						break;
					case "hasCompletedConversation":
						instance.hasCompletedConversation = reader.Read<System.Boolean>(ES3Type_bool.Instance);
						break;
					case "hasCompletedDailyTasks":
						instance.hasCompletedDailyTasks = reader.Read<System.Boolean>(ES3Type_bool.Instance);
						break;
					case "hasCompletedBattle":
						instance.hasCompletedBattle = reader.Read<System.Boolean>(ES3Type_bool.Instance);
						break;
					case "allDailyTasksForLevel":
						instance.allDailyTasksForLevel = reader.Read<System.Collections.Generic.List<DailyTask>>();
						break;
					default:
						reader.Skip();
						break;
				}
			}
		}
	}


	public class ES3UserType_LevelDataArray : ES3ArrayType
	{
		public static ES3Type Instance;

		public ES3UserType_LevelDataArray() : base(typeof(LevelData[]), ES3UserType_LevelData.Instance)
		{
			Instance = this;
		}
	}
}