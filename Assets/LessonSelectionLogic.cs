﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class LessonSelectionLogic : MonoBehaviour
{
    Screen_Guru guru;
    LessonDisplayTextLogic lessonDisplayTextLogic;
    

    [Header("Lesson Section")]
    public GameObject lessonSelectionPrefab;
    public Transform allLessonHolder;

    public List<LessonButton> allSpawnedButtons = new List<LessonButton>();

    private void OnEnable()
    {
        lessonDisplayTextLogic = lessonDisplayTextLogic ?? FindObjectOfType<LessonDisplayTextLogic>();
        LessonDisplayTextLogic.OnGuruLessonFinished += LessonDisplayTextLogic_OnGuruLessonFinished;
    }
    private void OnDisable()
    {
        LessonDisplayTextLogic.OnGuruLessonFinished += LessonDisplayTextLogic_OnGuruLessonFinished;
    }
    private void LessonDisplayTextLogic_OnGuruLessonFinished(int lessonIndex)
    {
        LessonButton lessonButton = allSpawnedButtons.SingleOrDefault(lb => lb.lesson.levelRequirement == lessonIndex);
        lessonButton.SetButtonGraphic(GetSpriteForButton(true));
    }

    public void Setup(List<Lesson> allLessons, Screen_Guru guru)
    {
        this.guru = guru;

        if(allSpawnedButtons.Count > 0)
        {
            ToggleAllButtons();
            return;
        }

        for (int i = 0; i < allLessons.Count; i++)
        {
            Lesson lesson = allLessons[i];
            LevelData levelData = guru.levelSaveDataManager.GetLevelDataByIndex(lesson.levelRequirement);
            bool hasCompletedLessonAlready = false;
            if (levelData)
                hasCompletedLessonAlready = levelData.HasFinishedGuru();
            CreateButton(allLessons[i], hasCompletedLessonAlready);
        }
    }

    public void CreateButton(Lesson lesson, bool hasCompletedAlready)
    {
        GameObject button = Instantiate(lessonSelectionPrefab, allLessonHolder);
        
        LessonButton buttonLogic = button.GetComponent<LessonButton>();
        buttonLogic.Setup(lesson);
        buttonLogic.SetButtonGraphic(GetSpriteForButton(hasCompletedAlready));

        allSpawnedButtons.Add(buttonLogic);

        buttonLogic.lessonButton.onClick.AddListener(() => { StartLesson(lesson); });

        button.gameObject.SetActive(buttonLogic.levelRequirement <= SaveData.PlayerLevel/*I want a gameManager.currentLevel CHANGE LATER */);
        //Add code here for disabling the button based on the level of the player
    }
    //[!!!]This will return a sprite later on
    public Color GetSpriteForButton(bool hasCompleted)
    {
        return hasCompleted ? guru.coloursForButtonsCompletion[0] : guru.coloursForButtonsCompletion[1];

    }

    public void StartLesson(Lesson lesson)
    {
        Screen_Guru.OnGuruLessonStateChange?.Invoke(true);

        lessonDisplayTextLogic.StartLesson(lesson);
    }

    public void ToggleAllButtons()
    {
        for (int i = 0; i < allSpawnedButtons.Count; i++)
        {
            allSpawnedButtons[i].gameObject.SetActive(allSpawnedButtons[i].levelRequirement <= SaveData.PlayerLevel/*I want a gameManager.currentLevel CHANGE LATER */);
        }
    }

   


}