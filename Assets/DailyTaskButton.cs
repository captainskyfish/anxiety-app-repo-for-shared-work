﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DailyTaskButton : MonoBehaviour
{
    Screen_HallOfHeroes hallOfHeroes;
    DailyTaskSaveData saveData;
    public Image taskButtonCompletedIcon;
    public Button finishTaskButton;
    public TextMeshProUGUI taskHeaderNameText;

    public void InitialiseButton(DailyTaskSaveData saveData, Screen_HallOfHeroes heroesScreen)
    {
        this.saveData = saveData;
        hallOfHeroes = heroesScreen;
        taskHeaderNameText.text = saveData.taskHeaderName;
        taskButtonCompletedIcon.gameObject.SetActive(saveData.isCompleted);

        ToggleButtonUse(!saveData.isCompleted);
    }

    public void ToggleButtonUse(bool isInteractable)
    {
        finishTaskButton.interactable = isInteractable;
        if (isInteractable)
        {
            finishTaskButton.onClick.AddListener(() =>
            {
                hallOfHeroes.TriggerPopup(saveData);
            });
        }
    }


}
