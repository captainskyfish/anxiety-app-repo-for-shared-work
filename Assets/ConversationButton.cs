﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ConversationButton : MonoBehaviour
{
    Screen_ConversationInstitute conversationInstitute;
    public int levelRequirement;
    public Button conversationButton;
    public Conversation conversation;
    public Image image;
    public TextMeshProUGUI conversationNameText;
    public bool isEnable = false;
    public bool isUnloked = false;

    private void Awake()
    {
        conversationButton = GetComponent<Button>();
        conversationInstitute ??= FindObjectOfType<Screen_ConversationInstitute>();
    }

    public void Setup(Conversation conversation, bool hasCompletedAlready)
    {
        this.conversation = conversation;
        levelRequirement = conversation.levelRequirement;

        SetupText(conversation);
        SetColor(hasCompletedAlready);
    }

    public void SetupText(Conversation conversation)
    {
        conversationNameText.text = $"{conversation.adviceName}";
    }

    public void SetColor(bool hasCompletedAlready)
    {
        image.color = hasCompletedAlready ? conversationInstitute.hasCompletedColours[1] : conversationInstitute.hasCompletedColours[0];
        if (hasCompletedAlready) isUnloked = true;
        else isUnloked = false;
    }
}
