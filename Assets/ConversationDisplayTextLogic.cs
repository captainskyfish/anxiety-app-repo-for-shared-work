using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ConversationDisplayTextLogic : MonoBehaviour
{
    public static event Action<int> OnConversationFinished;
    Utility_TextScrolling textScroller;

    Conversation currentConversation;
    string[] allAdviceText;

    int currentTextLineIndex;

    public Button nextTextButton;

    public bool inConversation;

    public void OnEnable()
    {
        textScroller = textScroller ?? FindObjectOfType<Utility_TextScrolling>();

        ToggleNextButton(false);

        SetupNextTextButton();

        Utility_TextScrolling.OnLineFinished += Utility_TextScrolling_OnLineFinished;
        Utility_TextScrolling.OnLineStarted += Utility_TextScrolling_OnLineStarted;
    }



    private void OnDisable()
    {
        Utility_TextScrolling.OnLineFinished -= Utility_TextScrolling_OnLineFinished;
        Utility_TextScrolling.OnLineStarted -= Utility_TextScrolling_OnLineStarted;
    }



    public void StartLesson(Conversation conversation)
    {
        inConversation = true;

        currentConversation = conversation;
        allAdviceText = conversation.allAdviceText;

        currentTextLineIndex = 0;

        SetupFirstLine();
    }

    public void SetupFirstLine()
    {
        textScroller.SetupText(currentConversation.allAdviceText[0]);
    }

    public void ShowNextLine()
    {
        string lineOfText = allAdviceText[currentTextLineIndex];
        textScroller.SetupText(lineOfText);
    }

    public void SetupNextTextButton()
    {
        nextTextButton.onClick.AddListener(ShowNextLine);
    }

    public void ToggleNextButton(bool isActive)
    {
        nextTextButton.gameObject.SetActive(isActive);
    }

    public bool IsEndOfText()
    {
        return currentTextLineIndex >= allAdviceText.Length;
    }

    private void Utility_TextScrolling_OnLineFinished()
    {
        currentTextLineIndex++;

        if (IsEndOfText())
        {
            OnConversationFinished?.Invoke(currentConversation.levelRequirement);
            Screen_ConversationInstitute.OnConversationStateChange?.Invoke(false);
            return;
        }

        ToggleNextButton(true);
    }

    private void Utility_TextScrolling_OnLineStarted()
    {
        ToggleNextButton(false);
    }

    public void FinishLesson()
    {
        inConversation = false;
    }
}