﻿using System;
using System.Collections;
using UnityEngine;

public static class SaveData
{

    public static int PlayerLevel
    {
        get => ES3.FileExists("SaveData") ? ES3.KeyExists("PlayerLevel", "SaveData") ? (int)ES3.Load("PlayerLevel", "SaveData") : 1 : 1;
        set
        {
            ES3.Save("PlayerLevel", value, "SaveData");
            EventManager.TriggerLevelUp(value);
        }
    }
    public static float CurrentCharacterSpeed
    {
        //get => ES3.FileExists("SaveData") ? ES3.KeyExists("CurrentTextSpeed", "SaveData") ? (float)ES3.Load("CurrentTextSpeed", "SaveData") : 0 : 0;
        get { return 0.005f; }
        set { ES3.Save("CurrentTextSpeed", value, "SaveData"); }
    }

    public static bool IsFirstTime
    {
        get => ES3.FileExists("SaveData") ? ES3.KeyExists("IsFirstTime", "SaveData") ? (bool)ES3.Load("IsFirstTime", "SaveData") : true : true;
        set
        {
            ES3.Save("IsFirstTime", value, "SaveData");
        }
    }

    public static void SetCurrentSpeed(Utility_TextScrolling.ScrollingSpeeds scrollingType)
    {
        switch(scrollingType)
        {
            case Utility_TextScrolling.ScrollingSpeeds.Slow: CurrentCharacterSpeed = 0.05f; break;
            case Utility_TextScrolling.ScrollingSpeeds.Normal: CurrentCharacterSpeed = 0.02f; break;
            case Utility_TextScrolling.ScrollingSpeeds.Fast: CurrentCharacterSpeed = 0.005f; break;
        }
    }

    public static bool CanAccessSignPedastal
    {
        get => ES3.FileExists("SaveData") ? ES3.KeyExists("CanAccessSignPedastal", "SaveData") ? (bool)ES3.Load("CanAccessSignPedastal", "SaveData") : true : true;
        set
        {
            ES3.Save("CanAccessSignPedastal", value, "SaveData");
        }
    }

    public static bool HasSignedPedastal
    {
        get => ES3.FileExists("SaveData") ? ES3.KeyExists("HasSignedPedastal", "SaveData") ? (bool)ES3.Load("HasSignedPedastal", "SaveData") : true : true;
        set
        {
            ES3.Save("HasSignedPedastal", value, "SaveData");
        }
    }

    public static bool HasEnteredBattleBefore
    {
        get => ES3.FileExists("SaveData") ? ES3.KeyExists("HasEnteredBattleBefore", "SaveData") ? (bool)ES3.Load("HasEnteredBattleBefore", "SaveData") : true : true;
        set
        {
            ES3.Save("HasEnteredBattleBefore", value, "SaveData");
        }
    }


}
