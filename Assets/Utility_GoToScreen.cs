using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Utility_GoToScreen : MonoBehaviour, IPointerClickHandler
{
    public ScreenManager.AllScreens screenToGoTo;

    public void OnPointerClick(PointerEventData eventData)
    {
        GoToScreen();
    }

    public void GoToScreen()
    {
        FindObjectOfType<ScreenManager>().GoToScreen(screenToGoTo);
    }
}
