using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelData : ScriptableObject
{
    public int levelNumber;
    public bool requireGuru;
    public bool requireDailyTasks;
    public bool requireBattle;

    public bool hasCompletedGuru;
    public bool hasCompletedDailyTasks;
    public bool hasCompletedBattle;
    public bool hasCompletedConversation;

    public List<DailyTask> allDailyTasksForLevel = new List<DailyTask>();

    public bool AllRequirementsMetForLevelUp()
    {
        LoadLevelData();
        if (requireGuru)
        {
            if(!HasFinishedGuru()) return false;
        }
        if (requireDailyTasks)
        {
            if (!HasFinishedDailyTasks()) return false;
        }
        if (requireBattle)
        {
            if (!HasCompletedBattle()) return false;
        }
        return true;
    }

    public bool HasFinishedGuru()
    {
        return hasCompletedGuru;
    }

    public bool HasFinishedConversation()
    {
        return hasCompletedConversation;
    }
    public bool HasFinishedDailyTasks()
    {
        List<DailyTaskSaveData> allDailyTasks = new List<DailyTaskSaveData>();
        allDailyTasksForLevel.ForEach(t => allDailyTasks.Add(t.GetSaveData(levelNumber)));

        int numberCompleted = 0;
        for (int i = 0; i < allDailyTasks.Count; i++)
        {
            numberCompleted += (allDailyTasks[i].isCompleted) ? 1:0;
        }

        return numberCompleted >= 2;
    }
    public bool HasCompletedBattle()
    {
        return hasCompletedBattle;
    }

    public void SetCompletedGuru()
    {
        if (requireGuru)
            hasCompletedGuru = true;

        SaveLevelData();
    }

    public void SetCompletedCombat()
    {
        if (requireBattle)
            hasCompletedBattle = true;

        SaveLevelData();
    }

    public void SaveLevelData()
    {
        ES3.Save($"Level_{levelNumber}", this, "LevelSaveData");
    }

    public void SetHasCompletedConversation()
    {
        hasCompletedConversation = true;
        SaveLevelData();
    }

    public void LoadLevelData()
    {
        LevelData levelData = (LevelData)ES3.Load($"Level_{levelNumber}", "LevelSaveData");
        //requireGuru             = levelData.requireGuru;
        //requireDailyTasks       = levelData.requireDailyTasks;
        //requireBattle           = levelData.requireBattle;
        hasCompletedGuru        = levelData.hasCompletedGuru;
        hasCompletedDailyTasks  = levelData.hasCompletedDailyTasks;
        hasCompletedBattle      = levelData.hasCompletedBattle;
        hasCompletedConversation = levelData.hasCompletedConversation;
    }

    public void ClearLevelData()
    {
        hasCompletedGuru = false;
        hasCompletedDailyTasks = false;
        hasCompletedBattle = false;
        hasCompletedConversation = false;
        SaveLevelData();
    }
}


