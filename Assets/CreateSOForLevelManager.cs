﻿using Databox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if USING_EDITOR

using UnityEditor;
//[CreateAssetMenu(fileName = "DailyRewardsManager", menuName = "Create Daily Rewards Manager")]
public class CreateSOForLevelManager : MonoBehaviour
{

  /*  public DataboxObject database;
    public const string AllLevelRequirementsKey = "AllLevelRequirements";
    public const string DailyMissionIndexKey= "DailyMission_";

    public List<DailyTask> allDailyTasks = new List<DailyTask>();

    public void Start()
    {
        if (!database.databaseLoaded) LoadDatabase();

        if (database.databaseLoaded) LoadForEachDatabase();
    }
    public void LoadDatabase()
    {
        database.LoadDatabase();
    }

    public void LoadForEachDatabase()
    {
        var data = database.GetEntriesFromTable(AllLevelRequirementsKey);
        int index = 1;
        foreach (var entry in data.Keys)
        {
            LevelData levelData = CreateLevelDataHolder();
            levelData.levelNumber = index;
            PopulateAsset(levelData, entry);

            CreateAsset(levelData, $"Level_{index}_Requirements");
            index++;
        }
        SaveAllAssets();
    }

    public LevelData CreateLevelDataHolder()
    {
        return ScriptableObject.CreateInstance<LevelData>();
    }

    public void CreateAsset(LevelData levelData, string name)
    {
        AssetDatabase.CreateAsset(levelData, $"Assets/LevelRequirements/{name}.asset");
    }

    public void PopulateAsset(LevelData levelData, string entry)
    {
        List<int> allTaskIndexes = new List<int>();
        for (int i = 0; i < 5; i++)
        {
            allTaskIndexes.Add(DataboxLib.GetIntData(database, AllLevelRequirementsKey, entry, DailyMissionIndexKey + (i + 1)));
        }

        levelData.allDailyTasksForLevel = GetAllTasksForLevel(allTaskIndexes);

    }

    public List<DailyTask> GetAllTasksForLevel(List<int> allIndexes)
    {
        List<DailyTask> toReturn = new List<DailyTask>();

        for (int i = 0; i < allDailyTasks.Count; i++)
        {
            if (allIndexes.Contains(allDailyTasks[i].dailyTaskLevelRequirement))
                toReturn.Add(allDailyTasks[i]);
        }

        return toReturn;
    }

    public void SaveAllAssets()
    {
        AssetDatabase.SaveAssets();
    }*/
}
#endif
