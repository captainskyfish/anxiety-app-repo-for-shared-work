using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ant_AnswerButton : MonoBehaviour
{
    Button button;
    Answer answer;
    Utility_TextScrolling scrollingText;

    bool isCorrect;

    int currentIndex;
    int maxIndex;

    public bool canPress;

    public void SetupButton(Answer answer)
    {
        FindReferences();
        this.answer = answer;
        isCorrect = answer.isCorrectAnswer;
        ClearButton();

        currentIndex = 0;
        maxIndex = answer.allAnswers.Count - 1;

        DisplayStartText();
        button.onClick.AddListener(ShowNextLine);
    }

    public void FindReferences()
    {
        scrollingText ??= GetComponentInChildren<Utility_TextScrolling>();
        button ??= GetComponent<Button>();

    }


    public void ShowNextLine()
    {

        if (!isCorrect)
        {
            EventManager.TriggerWrongAnswer();
            return;
        }

        EventManager.TriggerCorrectAnswer();

        ToggleButton(false);

        currentIndex++;

        if(currentIndex > maxIndex)
        {
            FinishedShowingNextText();
            print("No more text");
            return;
        }

        string text = answer.allAnswers[currentIndex];
        scrollingText.SetupText(text, FinishedShowingNextText);
    }

    public void FinishedShowingNextText()
    {
        print("Finished showing next text");
        if ((currentIndex) > maxIndex)
        {
            EventManager.TriggerFinishFight();
            return;
        }
        


        ToggleButton(true);
    }

    public void FinishedShowingStartText()
    {

        ToggleButton(true);
    }

    public void DisplayStartText()
    {
        ToggleButton(false);
        scrollingText.SetupText(answer.allAnswers[0], FinishedShowingStartText, true);
    }
    public void ToggleButton(bool isActive)
    {
        button.interactable = isActive;
    }

    public void ClearButton()
    {
        FindReferences();

        button.onClick.RemoveAllListeners();
        scrollingText.textToScroll.text = string.Empty;
        canPress = false;
        ToggleButton(false);
    }



}

