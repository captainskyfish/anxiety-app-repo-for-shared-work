using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Screen_MainMenu : ScreenLogic
{
    public static event Action<int> OnReadyToLevelUp;

    public LevelData currentLevel;
    DailySaveDataManager saveDataManager;

    public ToggleBuildings[] allToggleBuildings;

    public GameObject signPedastal;
    public SignPopup signPopup;

    public Button testingLevelUp;
    private void Awake()
    {
        saveDataManager ??= FindObjectOfType<DailySaveDataManager>();
        ToggleSignPopupBox(false);
    }

    private void OnEnable()
    {
        LevelUpManager.OnLevelUpTriggered += LevelUpEventTrigger;
    }

    private void OnDisable()
    {
        LevelUpManager.OnLevelUpTriggered -= LevelUpEventTrigger;
    }

    public void Start()
    {
        testingLevelUp.onClick.AddListener(OnTestingLeveleUpClick);

        if (!SaveData.CanAccessSignPedastal)
            signPedastal.SetActive(false);

        UpdateVisibilityOfBuildings();
    }

    int testCount;
    private void OnTestingLeveleUpClick()
    {
        if(testCount >= 5)
        {
            testCount = 0;
            StartCoroutine(LevelUpManager.instance.LevelUp());
            return;
        }
        testCount++;
    }

    public void LevelUpEventTrigger()
    {
        if (SaveData.CanAccessSignPedastal && !SaveData.HasSignedPedastal)
            signPedastal.SetActive(true);
        UpdateVisibilityOfBuildings();
    }

    public void UpdateVisibilityOfBuildings()
    {
        for (int i = 0; i < allToggleBuildings.Length; i++)
        {
            allToggleBuildings[i].ToggleBuilding();
        }
    }

    public void ToggleSignPopupBox(bool isActive)
    {
       
        signPopup.ToggleAgreeButtons( !SaveData.HasSignedPedastal);

        signPopup.gameObject.SetActive(isActive);
    }

    public void PlayerSignsPedastalAgreement()
    {
        SaveData.HasSignedPedastal = true;
        ToggleSignPopupBox(false);
        FindObjectOfType<LevelUpManager>().LevelUpToLevelTwo();
    }

    public override void Setup()
    {
        currentLevel = saveDataManager.GetLevelDataByIndex(SaveData.PlayerLevel);
        CheckRequirementsAreMet();
        HighlightInstitute();
    }

    public void CheckRequirementsAreMet()
    {
        if (currentLevel.AllRequirementsMetForLevelUp())
            OnReadyToLevelUp?.Invoke(SaveData.PlayerLevel);
    }
    private void HighlightInstitute()
    {
        if(LevelUpManager.highlitedInstitute == 0)
        {
            LevelUpManager.instance.WithoutHighlightedConversationInst();
        }
        else
        {
            StartCoroutine(LevelUpManager.instance.HighlightingConversationInst(0));
        }
    }
}
