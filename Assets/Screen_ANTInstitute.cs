using Databox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Screen_ANTInstitute : ScreenLogic
{
    const string ANTInstituteKey = "ANTInstitute";
    const string LevelRequirementKey = "LevelRequirement";
    const string DebreifingKey = "DebreifingText";
    const string MonsterNameKey = "MonsterName";
    const string MonsterTextKey = "MonsterText";
    const string WrongAnswerKey = "WrongAnswer_";
    const string CorrectAnswerKey = "CorrectAnswer_";

    public Screen_ANTBattle antBattle;
    DailySaveDataManager saveDataManager;
    public DataboxObject ANTInstituteDatabase;
    public List<Fight> allFights = new List<Fight>();

    [Header("All Battle Display Section")]
    public Transform displayParent;
    public ANT_GoToFight fightButtonPrefab;
    public List<ANT_GoToFight> allSpawnedBattleButtons = new List<ANT_GoToFight>();
    public SpeechBubble speechBubble;
    public GameObject backButton;
    public Color[] fightStatusColors;
    public string introductionText;

    #region ---------- Introductory Button ----------
    [Header("Introductory Button")]
    public Transform introductoryBar;
    public GameObject introductoryPrefab;
    #endregion


    public override void Setup()
    {
        saveDataManager = FindObjectOfType<DailySaveDataManager>();

        if (!ANTInstituteDatabase.databaseLoaded) LoadDatabase();

        if (ANTInstituteDatabase.databaseLoaded) ReadFromDatabase();

        ClearAllButtons();
        CreateAllButtons();

        backButton.SetActive(true);

        if (!SaveData.HasEnteredBattleBefore)
            FirstTimeOpenLogic();

    }

    public void FirstTimeOpenLogic()
    {
        SaveData.HasEnteredBattleBefore = true;
        backButton.SetActive(false);
        speechBubble.StartSpeech(introductionText);
    }

    public void LoadDatabase()
    {
        ANTInstituteDatabase.LoadDatabase();
    }

    public void ReadFromDatabase()
    {
        if (allFights.Count > 0)
            return;

        var data = ANTInstituteDatabase.GetEntriesFromTable(ANTInstituteKey);

        foreach (var entry in data.Keys.Reverse())
        {
            Fight fight = new Fight();
            Question question = new Question();
            List<Answer> allAnswers = new List<Answer>();

            int levelReq = 0;
            string monsterName = string.Empty;
            string monsterText = string.Empty;
            string debriefingText = string.Empty;

            levelReq = GetIntData(ANTInstituteDatabase, ANTInstituteKey, entry, LevelRequirementKey);
            monsterName = GetStringData(ANTInstituteDatabase, ANTInstituteKey, entry, MonsterNameKey);
            monsterText = GetStringData(ANTInstituteDatabase, ANTInstituteKey, entry, MonsterTextKey);
            debriefingText = GetStringData(ANTInstituteDatabase, ANTInstituteKey, entry, DebreifingKey);

            for (int i = 0; i < 2; i++)
            {
                allAnswers.Add(new Answer());
                allAnswers[i].allAnswers.Add(GetStringData(ANTInstituteDatabase, ANTInstituteKey, entry, WrongAnswerKey + i));
            }

            allAnswers.Add(new Answer());
            for (int i = 0; i < 4; i++)
            {
                string text = GetStringData(ANTInstituteDatabase, ANTInstituteKey, entry, CorrectAnswerKey + i);
                if (text == "N/A") continue;

                allAnswers[2].allAnswers.Add(text);
            }
            allAnswers[2].isCorrectAnswer = true;

            fight.questionData = question;
            fight.questionData.question = monsterText;
            fight.debreifingText = debriefingText;
            fight.levelRequirement = levelReq;
            fight.monsterName = monsterName;
            fight.questionData.allAnswers = allAnswers;

            allFights.Add(fight);
        }

    }

    public void SetupAntBattle(Fight fight)
    {
        antBattle.InitialiseScreen(fight);
    }

    public void ClearAllButtons()
    {
        foreach (var button in allSpawnedBattleButtons.ToList())
        {
            Destroy(button.gameObject);
        }
        allSpawnedBattleButtons.Clear();
    }

    public void CreateAllButtons()
    {
        for (int i = 0; i < allFights.Count; i++)
        {
            ANT_GoToFight fightButton = CreateButton(allFights[i], i);
            GameObject buttonObj = fightButton.gameObject;
            LevelData currentLevelData = saveDataManager.GetLevelDataByIndex(allFights[i].levelRequirement);
            buttonObj.SetActive(SaveData.PlayerLevel >= allFights[i].levelRequirement);

            fightButton.SetColourOfButton(currentLevelData.hasCompletedBattle);
        }
        CreateInroductoryBtn();
    }
    private void CreateInroductoryBtn()
    {
        var gm = GameObject.Find("Content/IntroductoryButton");
        if (gm != null) Destroy(gm);
        GameObject lessonButton = Instantiate(introductoryPrefab, introductoryBar.position, Quaternion.identity, introductoryBar);
        lessonButton.name = "IntroductoryButton";
        lessonButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            backButton.SetActive(false);
            speechBubble.StartSpeech(introductionText);
        });
    }

    public ANT_GoToFight CreateButton(Fight fight, int i)
    {
        ANT_GoToFight fightButton = Instantiate(fightButtonPrefab.gameObject, displayParent).GetComponent<ANT_GoToFight>();

        fightButton.SetupButton(fight, i);
        allSpawnedBattleButtons.Add(fightButton);
        return fightButton;
    }

    #region DatabaseStuff
    public static int GetIntData(DataboxObject database, string tableID, string entryID, string valueID)
    {
        IntType _value;
        if (database.TryGetData<IntType>(tableID, entryID, valueID, out _value))
        {
            return _value.Value;
        }
        else
        {
            return 0;
        }
    }

    private static string GetStringData(DataboxObject database, string tableID, string entryID, string valueID)
    {
        StringType _StringType;
        if (database.TryGetData<StringType>(tableID, entryID, valueID, out _StringType))
        {
            return _StringType.Value;
        }
        else
        {
            return null;
        }
    }
    #endregion DatabaseStuff

}
[System.Serializable]
public class Fight
{
    public string monsterName;
    public int levelRequirement;
    public string debreifingText;
    public Question questionData;
    internal int monsterImgCounter;

}
[System.Serializable]
public class Question
{
    public string question;
    public List<Answer> allAnswers = new List<Answer>();
}

[System.Serializable]
public class Answer
{
    public bool isCorrectAnswer;
    public List<string> allAnswers = new List<string>();
}
