﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ContinousTextBox : MonoBehaviour
{
    Utility_TextScrolling textScroller;

    Action callback;
    string[] allText;
    int currentTextLineIndex;

    public Button nextTextButton;

    public bool inLesson;

    public void OnEnable()
    {
        textScroller = textScroller ?? FindObjectOfType<Utility_TextScrolling>();

        ToggleNextButton(false);

        SetupNextTextButton();

        Utility_TextScrolling.OnLineFinished += Utility_TextScrolling_OnLineFinished;
        Utility_TextScrolling.OnLineStarted += Utility_TextScrolling_OnLineStarted;
    }



    private void OnDisable()
    {
        Utility_TextScrolling.OnLineFinished -= Utility_TextScrolling_OnLineFinished;
        Utility_TextScrolling.OnLineStarted -= Utility_TextScrolling_OnLineStarted;
    }



    public void StartTextBox(string[] allText, Action callback = null)
    {
        inLesson = true;

        this.allText = allText;

        this.callback = null;
        if (callback != null) this.callback = callback;

        currentTextLineIndex = 0;

        SetupFirstLine();
    }

    public void SetupFirstLine()
    {
        textScroller.SetupText(allText[0]);
    }

    public void ShowNextLine()
    {
        if (IsEndOfText())
            return;

        string lineOfText = allText[currentTextLineIndex];
        textScroller.SetupText(lineOfText);
    }

    public void SetupNextTextButton()
    {
        nextTextButton.onClick.AddListener(ShowNextLine);
    }

    public void ToggleNextButton(bool isActive)
    {
        nextTextButton.gameObject.SetActive(isActive);
    }

    public bool IsEndOfText()
    {
        return currentTextLineIndex >= allText.Length;
    }

    private void Utility_TextScrolling_OnLineFinished()
    {
        currentTextLineIndex++;

        if (IsEndOfText())
        {
            callback?.Invoke();
            return;
        }

        ToggleNextButton(true);
    }



    private void Utility_TextScrolling_OnLineStarted()
    {
        ToggleNextButton(false);
    }

    public void FinishLesson()
    {
        inLesson = false;
    }
}
