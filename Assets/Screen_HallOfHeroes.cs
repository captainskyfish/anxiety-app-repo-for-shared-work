using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Screen_HallOfHeroes : ScreenLogic
{

    public DailySaveDataManager saveDataManager;
    public DailyTaskPopupWindow dailyTaskPopup;
    public Transform dailyTaskParent;
    public GameObject dailyTaskButton;
    public List<DailyTaskButton> spawnedButtons = new List<DailyTaskButton>();
    public List<DailyTaskSaveData> saveDataToShow = new List<DailyTaskSaveData>();

    public override void Setup()
    {
        saveDataManager ??= FindObjectOfType<DailySaveDataManager>();
        ClearAllButtons();
        GenerateButtons();
    }

    private void OnEnable()
    {
        DailySaveDataManager.OnDailyMissionCompleted += DailySaveDataManager_OnDailyMissionCompleted;
    }

    private void OnDisable()
    {
        DailySaveDataManager.OnDailyMissionCompleted -= DailySaveDataManager_OnDailyMissionCompleted;
        MainMenuScript.instance.DisableDailyTaskPanel();
    }

    public void GenerateButtons()
    {
        saveDataToShow = saveDataManager.allSaveData.Where(d => d.levelIndex == SaveData.PlayerLevel).ToList();

        for (int i = 0; i < saveDataToShow.Count; i++)
        {
            DailyTaskButton taskButton = CreateTaskButton();
            taskButton.InitialiseButton(saveDataToShow[i], this);
            spawnedButtons.Add(taskButton);
        }
    }

    public DailyTaskButton CreateTaskButton()
    {
        return Instantiate(dailyTaskButton, dailyTaskParent).GetComponent<DailyTaskButton>();
    }

    public void TriggerPopup(DailyTaskSaveData saveData)
    {
        dailyTaskPopup.InitialisePopup(saveData, this);
    }

    public void CompleteDailyTask(DailyTaskSaveData saveDataToFinish)
    {
        saveDataToFinish.isCompleted = true;
        FindObjectOfType<DailySaveDataManager>().SaveDailyTask(saveDataToFinish, true);
        if (FindObjectOfType<DailySaveDataManager>().GetLevelDataByIndex(saveDataToFinish.levelIndex).HasFinishedDailyTasks())
        {
            HideAllButtons();
        }
    }

    public void HideAllButtons()
    {
        foreach (var button in spawnedButtons)
        {
            button.finishTaskButton.interactable = false;
        }
    }

    public void ClearAllButtons()
    {
        foreach (var button in spawnedButtons.ToList())
        {
            Destroy(button.gameObject);
        }
        spawnedButtons.Clear();
    }

    public void UpdateButtons()
    {
        ClearAllButtons();
        GenerateButtons();
    }

    private void DailySaveDataManager_OnDailyMissionCompleted()
    {
        UpdateButtons();   
    }
}
