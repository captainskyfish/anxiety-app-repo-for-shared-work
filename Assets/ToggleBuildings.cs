﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleBuildings : MonoBehaviour
{
    public int levelRequirement;
    public Image building;
    public void ToggleBuilding()
    {
        building.gameObject.SetActive(SaveData.PlayerLevel >= levelRequirement);
    }
}