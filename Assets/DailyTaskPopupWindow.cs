﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DailyTaskPopupWindow : MonoBehaviour
{
    Screen_HallOfHeroes hallOfHeroes;

    public GameObject popup;
    public DailyTaskSaveData saveData;
    public TextMeshProUGUI headerText;
    public TextMeshProUGUI descriptionText;
    public Button closeButton;
    public Button completeButton;

    private int requreTaskPerDay = 2;
    public void InitialisePopup(DailyTaskSaveData data, Screen_HallOfHeroes hallOfHeroes)
    {
        this.hallOfHeroes = hallOfHeroes;
        saveData = data;

        headerText.text = data.taskHeaderName;
        descriptionText.text = data.taskDescription;

        TogglePopup(true);

        InitCloseButton();
        InitCompleteButton();
    }

    public void TogglePopup(bool isActive)
    {
        popup.SetActive(isActive);
    }

    public void InitCloseButton()
    {
        closeButton.onClick.RemoveAllListeners();
        closeButton.onClick.AddListener(() =>
        {
            TogglePopup(false);
        });
    }

    public void InitCompleteButton()
    {
        completeButton.onClick.RemoveAllListeners();
        completeButton.onClick.AddListener(() =>
        {
            if (MainMenuScript.DailyTaskCompleteCount < requreTaskPerDay)
            {
                MainMenuScript.DailyTaskCompleteCount++;
                if (MainMenuScript.DailyTaskCompleteCount == requreTaskPerDay)
                {
                    MainMenuScript.instance.OnSetTimer();
                }

                hallOfHeroes.CompleteDailyTask(saveData);
                TogglePopup(false);
            }
            else if (MainMenuScript.DailyTaskCompleteCount >= requreTaskPerDay)
            {
                if (!MainMenuScript.instance.IsTimerEnded())
                {
                    MainMenuScript.instance.EnableDailyTaskPanel();
                    TogglePopup(false);
                }
            }
        });
    }


}