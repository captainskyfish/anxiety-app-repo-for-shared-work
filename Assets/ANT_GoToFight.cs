using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ANT_GoToFight : MonoBehaviour
{
    public Fight fightData;
    Screen_ANTInstitute antInstitute;
    Button button;
    public TextMeshProUGUI fightDisplayText;


    public void SetupButton(Fight fight, int i)
    {
        antInstitute = FindObjectOfType<Screen_ANTInstitute>();
        button ??= GetComponent<Button>();
        fightDisplayText ??= GetComponentInChildren<TextMeshProUGUI>();
        fightData = fight;

        ScreenManager screenManager = FindObjectOfType<ScreenManager>();
        fight.monsterImgCounter = i;

        fightDisplayText.text = fight.monsterName;

        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => { screenManager.GoToScreen(ScreenManager.AllScreens.ANTCombat); antInstitute.SetupAntBattle(fightData); });
    }

    public void SetColourOfButton(bool isFinishedAlready)
    {
        GetComponent<Image>().color = isFinishedAlready ? antInstitute.fightStatusColors[0] : antInstitute.fightStatusColors[1];
    }

}
