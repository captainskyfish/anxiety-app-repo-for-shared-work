﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LessonDisplayTextLogic : MonoBehaviour
{
    public static event Action<int> OnGuruLessonFinished;
    Utility_TextScrolling textScroller; 
    Lesson lesson;

    Lesson currentLesson;
    [SerializeField]string[] allLessonText;

    [SerializeField] int currentTextLineIndex;

    public Button nextTextButton;

    public bool inLesson;

    public void OnEnable()
    {
        textScroller = textScroller ?? FindObjectOfType<Utility_TextScrolling>();

        ToggleNextButton(false);

        SetupNextTextButton();

        Utility_TextScrolling.OnLineFinished += Utility_TextScrolling_OnLineFinished;
        Utility_TextScrolling.OnLineStarted += Utility_TextScrolling_OnLineStarted;
    }

   

    private void OnDisable()
    {
        Utility_TextScrolling.OnLineFinished -= Utility_TextScrolling_OnLineFinished;
        Utility_TextScrolling.OnLineStarted -= Utility_TextScrolling_OnLineStarted;
    }


    public void StartLesson(Lesson lesson)
    {
        this.lesson = lesson;
        inLesson = true;

        currentLesson = lesson;
        allLessonText = lesson.allLessonText;

        currentTextLineIndex = 0;

        SetupFirstLine();
    }

    public void SetupFirstLine()
    {
        textScroller.SetupText(currentLesson.allLessonText[0]);
    }

    public void ShowNextLine()
    {
        if (IsEndOfText())
            return;

        string lineOfText = allLessonText[currentTextLineIndex];
        textScroller.SetupText(lineOfText);
    }

    public void SetupNextTextButton()
    {
        nextTextButton.onClick.AddListener(ShowNextLine);
    }

    public void ToggleNextButton(bool isActive)
    {
        nextTextButton.gameObject.SetActive(isActive);
    }

    public bool IsEndOfText()
    {
        return currentTextLineIndex >= allLessonText.Length;
    }

    private void Utility_TextScrolling_OnLineFinished()
    {
        currentTextLineIndex++;

        if (IsEndOfText())
        {
            OnGuruLessonFinished?.Invoke(lesson.levelRequirement);
            Screen_Guru.OnGuruLessonStateChange?.Invoke(false);
            print("finished, announce event to trigger finish of lesson");

            return;
        }

        ToggleNextButton(true);
    }

  

    private void Utility_TextScrolling_OnLineStarted()
    {
        ToggleNextButton(false);
    }

    public void FinishLesson()
    {
        inLesson = false;
    }
}
