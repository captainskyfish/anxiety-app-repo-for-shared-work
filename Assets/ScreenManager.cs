using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenManager : MonoBehaviour
{
    public enum AllScreens
    {
        MainMenu,
        Guru,
        HallOfHeroes,
        ConversationInstitute,
        ANTInstitute,
        ANTCombat,
        ANTDebriefing,
    }
    public enum Orientation
    {
        Portrait,
        Landscape,
    }
    private void Start()
    {
        GoToScreen(AllScreens.MainMenu);
    }

    public ScreenLogic[] allScreens;

    public void GoToScreen(AllScreens screen)
    {
  
        for (int i = 0; i < allScreens.Length; i++)
        {
            ScreenLogic logic = allScreens[i];
            if(logic.screen == screen)
            {
                SetOrientation(logic.orientation);
                logic.gameObject.SetActive(true);
                logic.SetupScreen(this);
            }
            else
            {
                logic.gameObject.SetActive(false);
            }
        }
    }

    public void SetOrientation(Orientation orientation)
    {
        switch (orientation)
        {
            case Orientation.Portrait: Screen.orientation = ScreenOrientation.Portrait; FindObjectOfType<SafeResizer>().ResetSize(); break;
            case Orientation.Landscape: Screen.orientation = ScreenOrientation.Landscape; FindObjectOfType<SafeResizer>().ResetSize(); break;
        }
    }
}
