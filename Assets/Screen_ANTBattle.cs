using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Screen_ANTBattle : ScreenLogic
{
    public static Action<int> OnCombatFinished;


    [SerializeField] Image battleBackgroundImageObject;
    [SerializeField] Image battleMonsterImageObject;
    [SerializeField] Sprite[] allBattleBackgroundImages;
    [SerializeField] Sprite[] allBattleMonsterImages;

    public Fight fight;
    public ANTBattleUI antBattleUI;
    public Screen_ANTDebreifing debriefing;
    public Monster monster;

    public float startTransitionTime;
    public float finishTransitionTime;


    private void OnEnable()
    {
        EventManager.OnFinishFight += EventManager_OnFinishFight;
        EventManager.OnFinishFight += EventManager_OnCorrectAnswer;
        EventManager.OnCorrectAnswer += EventManager_OnCorrectAnswer;
        EventManager.OnWrongAnswer += EventManager_OnWrongAnswer;
    }


    private void OnDisable()
    {
        EventManager.OnFinishFight -= EventManager_OnCorrectAnswer;
        EventManager.OnFinishFight -= EventManager_OnFinishFight;
        EventManager.OnCorrectAnswer -= EventManager_OnCorrectAnswer;
        EventManager.OnWrongAnswer -= EventManager_OnWrongAnswer;
    }

    public void InitialiseScreen(Fight fightData)
    {
        fight = fightData;
        antBattleUI ??= FindObjectOfType<ANTBattleUI>();
        antBattleUI.InitialiseFight(fight);
        monster.Initialise(fight.questionData.allAnswers.SingleOrDefault(a => a.isCorrectAnswer).allAnswers.Count);
        EventManager.TriggerFightStart();

        // Determine random monster
        if (allBattleMonsterImages != null && battleMonsterImageObject != null)
        {
            if (allBattleMonsterImages.Length > 0)
            {
                battleMonsterImageObject.sprite = allBattleMonsterImages[fight.monsterImgCounter];
            }
        }

        // Determine random screen
        if (allBattleBackgroundImages != null && battleBackgroundImageObject != null)
        {
            if (allBattleBackgroundImages.Length > 0)
            {
                battleBackgroundImageObject.sprite = allBattleBackgroundImages[fight.monsterImgCounter];
            }
        }
    }

    public override void Setup()
    {


    }

    public void LeaveFightTransition()
    {
        debriefing.fight = fight;
        screenManager.GoToScreen(ScreenManager.AllScreens.ANTDebriefing);
    }

    public IEnumerator IFinishFight()
    {
        yield return new WaitForSeconds(2);

        float time = 0;
        DOTween.To(() => time, x => time = x, finishTransitionTime, finishTransitionTime).OnComplete(() => { LeaveFightTransition(); });
        antBattleUI.fadeScreen.DOFade(1, finishTransitionTime);
        antBattleUI.transform.DOShakePosition(finishTransitionTime, 30);

        OnCombatFinished?.Invoke(fight.levelRequirement);
    }

    private void EventManager_OnFinishFight()
    {
        StartCoroutine(IFinishFight());
    }



    private void EventManager_OnWrongAnswer()
    {
        antBattleUI.transform.DOShakePosition(0.4f, 20);
    }

    private void EventManager_OnCorrectAnswer()
    {
        monster.Shrink();
        monster.FlashDamage();
    }

}
