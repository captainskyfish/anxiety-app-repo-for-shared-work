using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TopBarController : MonoBehaviour
{
    public TextMeshProUGUI levelText;
    public Image levelBar;
    

    public void OnEnable()
    {
        EventManager.OnPlayerLevelUp += SetLevelNumber;     
    }


    private void OnDisable()
    {
        EventManager.OnPlayerLevelUp -= SetLevelNumber;     
    }

    private void Start()
    {
        SetLevelNumber(SaveData.PlayerLevel);
    }

    public void SetLevelBarPercentage(float percentage)
    {
        levelBar.fillAmount = percentage;
    }

    public void SetLevelNumber(object obj = null)
    {
        levelText.text = ((int)obj).ToString();
    }

    public void OnExitClick()
    {
        Application.Quit();
    }
}
