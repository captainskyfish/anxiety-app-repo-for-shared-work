using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class ConversationSelectionLogic : MonoBehaviour
{
    Screen_ConversationInstitute conversation;
    ConversationDisplayTextLogic conversationDisplayTextLogic;

    [Header("Lesson Section")]
    public GameObject lessonSelectionPrefab;
    public Transform allLessonHolder;

    public List<ConversationButton> allSpawnedButtons = new List<ConversationButton>();

    private void OnEnable()
    {
        conversationDisplayTextLogic = conversationDisplayTextLogic ?? FindObjectOfType<ConversationDisplayTextLogic>();
        ConversationDisplayTextLogic.OnConversationFinished += ConversationDisplayTextLogic_OnConversationFinished;
    }

    private void OnDisable()
    {
        ConversationDisplayTextLogic.OnConversationFinished -= ConversationDisplayTextLogic_OnConversationFinished;

    }

    private void ConversationDisplayTextLogic_OnConversationFinished(int conversationIndex)
    {
        ConversationButton convoButton = allSpawnedButtons.SingleOrDefault(cb => cb.conversation.levelRequirement == conversationIndex);
        convoButton.SetColor(true);
        convoButton.isUnloked = true;
        conversation.levelSaveDataManager.GetLevelDataByIndex(conversationIndex).SetHasCompletedConversation();
        conversation.FinalHighlightInts();
    }

    public void Setup(List<Conversation> allLessons)
    {
        conversation = FindObjectOfType<Screen_ConversationInstitute>();
        if (allSpawnedButtons.Count > 0)
        {
            ToggleAllButtons();
            return;
        }

        for (int i = 0; i < allLessons.Count; i++)
        {
           
            Conversation conversation = allLessons[i];
            LevelData levelData = this.conversation.levelSaveDataManager.GetLevelDataByIndex(conversation.levelRequirement);
            bool hasCompletedLessonAlready = false;
            if (levelData)
                hasCompletedLessonAlready = levelData.HasFinishedConversation();
            CreateButton(allLessons[i], hasCompletedLessonAlready);
            
        }
    }

    public void CreateButton(Conversation conversation, bool hasCompletedAlready)
    {
        GameObject button = Instantiate(lessonSelectionPrefab, allLessonHolder);
        ConversationButton buttonLogic = button.GetComponent<ConversationButton>();
        buttonLogic.Setup(conversation, hasCompletedAlready);

        allSpawnedButtons.Add(buttonLogic);

        buttonLogic.conversationButton.onClick.AddListener(() => { StartLesson(conversation); });

        bool isvalue = buttonLogic.levelRequirement <= SaveData.PlayerLevel;
        button.gameObject.SetActive(isvalue/*I want a gameManager.currentLevel CHANGE LATER */);
        button.GetComponent<ConversationButton>().isEnable = isvalue;
        //Add code here for disabling the button based on the level of the player
    }

    public void StartLesson(Conversation conversation)
    {
        conversationDisplayTextLogic.StartLesson(conversation);
        Screen_ConversationInstitute.OnConversationStateChange?.Invoke(true);

    }

    public void ToggleAllButtons()
    {
        for (int i = 0; i < allSpawnedButtons.Count; i++)
        {
            bool isvalue = allSpawnedButtons[i].levelRequirement <= SaveData.PlayerLevel;
            allSpawnedButtons[i].gameObject.SetActive(isvalue/*I want a gameManager.currentLevel CHANGE LATER */);
            allSpawnedButtons[i].isEnable = (isvalue);
        }
    }




}