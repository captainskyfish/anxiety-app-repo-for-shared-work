using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ANTBattleUI : MonoBehaviour
{


    public Screen_ANTBattle antBattle;
    public Utility_TextScrolling monsterText;

    public Ant_AnswerButton[] allAnswerButtons;


    public Image fadeScreen;
    Fight fight;

    private void OnEnable()
    {
        EventManager.OnMonsterFinishedTalking += Event_OnMonsterFinishedTalking;
        EventManager.OnFightStart += EventManager_OnFightStart;
    }



    private void OnDisable()
    {
        EventManager.OnFightStart -= EventManager_OnFightStart;
        EventManager.OnMonsterFinishedTalking -= Event_OnMonsterFinishedTalking;
    }

    public void InitialiseFight(Fight fight)
    {
        antBattle ??= GetComponent<Screen_ANTBattle>();
        this.fight = fight;

        monsterText.textToScroll.text = "";
        ClearAllButtons();

        float time = 0;
        DOTween.To(() => time, x => time = x, antBattle.startTransitionTime, antBattle.startTransitionTime).OnComplete(() => { SetupMonsterText(fight.questionData.question); });
    }

    public void SetupMonsterText(string text)
    {
        monsterText.SetupText(text, Callback_FinishMonsterText);
    }

    public void Callback_FinishMonsterText()
    {
        EventManager.TriggerMonsterFinishTalking();
    }

    public void TriggerButtons()
    {
        StartCoroutine(SetupAllButtons());
    }

    public IEnumerator SetupAllButtons()
        {
            List<int> numberOfAnsPermanent = new List<int>();

            var numberOfAnsTemporary = Enumerable.Range(0, fight.questionData.allAnswers.Count).ToList();
            var rnd = new System.Random();
            var randomized = numberOfAnsTemporary.OrderBy(item => rnd.Next());
            foreach (var item in randomized)
            {
                numberOfAnsPermanent.Add(item);
            }

            for (int i = 0; i < fight.questionData.allAnswers.Count; i++)
            {
                int rand = UnityEngine.Random.Range(0, numberOfAnsPermanent.Count);
                Answer answer = fight.questionData.allAnswers[numberOfAnsPermanent[rand]];
                Ant_AnswerButton button = allAnswerButtons[i];

                button.SetupButton(answer);
                numberOfAnsPermanent.RemoveAt(rand);
                yield return new WaitForSeconds(0.5f);
            }
        }

    public void DeactivateAllButtons()
    {
        for (int i = 0; i < fight.questionData.allAnswers.Count; i++)
        {
            Ant_AnswerButton button = allAnswerButtons[i];
            button.ToggleButton(false);

        }
    }

    public void ClearAllButtons()
    {
        allAnswerButtons.ToList().ForEach(b => b.ClearButton());
    }

    private void Event_OnMonsterFinishedTalking()
    {
        TriggerButtons();
    }

    private void EventManager_OnFightStart()
    {
        print("fight start");
        fadeScreen.DOFade(1, 0);
        fadeScreen.DOFade(0, antBattle.startTransitionTime);
    }
}
