﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Monster : MonoBehaviour
{
    Image image;
    RectTransform rect;
    public int numberOfLives;
    public void Awake()
    {
        rect ??= GetComponent<RectTransform>();
        image ??= GetComponent<Image>();
    }

    public void Initialise(int numberOfLives)
    {
        ResetValues();
        this.numberOfLives = numberOfLives;
    }

    private void ResetValues()
    {
        rect.localScale = Vector2.one;
    }

    public void Shrink()
    {
        float currentScaleOfMonster = rect.localScale.x;
        float deductionAmount = 1 / (float)numberOfLives;
        rect.DOScale(Vector2.one * (currentScaleOfMonster - deductionAmount), 0.4f);
    }

    public void FlashDamage()
    {
        Sequence flashSequence = DOTween.Sequence();

        flashSequence.Append(image.DOFade(0, 0.1f));
        flashSequence.Append(image.DOFade(1, 0.1f));
        flashSequence.Append(image.DOFade(0, 0.1f));
        flashSequence.Append(image.DOFade(1, 0.1f));
        flashSequence.Append(image.DOFade(0, 0.1f));
        flashSequence.Append(image.DOFade(1, 0.1f));

        flashSequence.Play();
    }
}