using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
public class Utility_TextScrolling : MonoBehaviour
{
    public enum ScrollingSpeeds
    {
        Slow = 1,
        Normal,
        Fast
    }

    public delegate void TextEvents();
    public static event TextEvents OnLineStarted;
    public static event TextEvents OnLineFinished;

    public TextMeshProUGUI textToScroll;

    Action callback;

    public void Awake()
    {
        textToScroll = GetComponent<TextMeshProUGUI>();
        ResetText();
    }
    private void OnEnable()
    {
        ResetText();
    }
    public void SetupText(string textToDisplay, Action callback = null, bool isInstant = false)
    {
        ResetText();
        OnLineStarted?.Invoke();
        this.callback = null;
        if (callback != null) this.callback = callback;
        ScrollText(textToDisplay, isInstant);
    }

    public Tween scrollingTextTween;
    public void ScrollText(string textToDisplay, bool isInstant)
    {
        float speed = (isInstant) ? 0 : SaveData.CurrentCharacterSpeed * textToDisplay.Length;
        scrollingTextTween = textToScroll.DOText(textToDisplay, speed).SetEase(Ease.Linear).OnComplete(() =>
        {
            callback?.Invoke(); 
            OnLineFinished?.Invoke();
        });
    }
    public void ResetText()
    {
        textToScroll.text = "";
        if (scrollingTextTween != null)
            scrollingTextTween.Kill();

        scrollingTextTween = null;
    }
}
