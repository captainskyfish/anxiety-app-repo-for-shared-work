﻿using UnityEditor;

[CustomEditor(typeof(LevelData)), ]
[CanEditMultipleObjects]
public class LevelDataEditor : Editor
{
    SerializedProperty levelNumber;
    SerializedProperty requireGuru;
    SerializedProperty requireDailyTasks;
    SerializedProperty requireBattle;

    SerializedProperty hasCompletedGuru;
    SerializedProperty hasCompletedDailyTasks;
    SerializedProperty hasCompletedBattle;
    SerializedProperty hasCompletedConversation;

    SerializedProperty allDailyTasksForLevel;
    public void FindPropertyReferences()
    {
        levelNumber = serializedObject.FindProperty("levelNumber");
        requireGuru = serializedObject.FindProperty("requireGuru");
        requireDailyTasks = serializedObject.FindProperty("requireDailyTasks");
        requireBattle = serializedObject.FindProperty("requireBattle");
        hasCompletedGuru = serializedObject.FindProperty("hasCompletedGuru");
        hasCompletedDailyTasks = serializedObject.FindProperty("hasCompletedDailyTasks");
        hasCompletedBattle = serializedObject.FindProperty("hasCompletedBattle");
        hasCompletedConversation = serializedObject.FindProperty("hasCompletedConversation");
        allDailyTasksForLevel = serializedObject.FindProperty("allDailyTasksForLevel");
    }

    public override void OnInspectorGUI()
    {


        FindPropertyReferences();

        serializedObject.Update();
        

        EditorGUI.indentLevel = 0;
        CustomIntField("Level Number", levelNumber);
        
        CustomBoolField("requireGuru", requireGuru);
        if (requireGuru.boolValue)
        {
            EditorGUI.indentLevel = 1;
            CustomBoolField("hasCompletedGuru", hasCompletedGuru);
        }

        EditorGUI.indentLevel = 0;
        CustomBoolField("requireDailyTasks", requireDailyTasks);
        if (requireDailyTasks.boolValue)
        {
            EditorGUI.indentLevel = 1;
            CustomBoolField("hasCompletedDailyTasks", hasCompletedDailyTasks);

        }

        EditorGUI.indentLevel = 0;
        CustomBoolField("requireBattle", requireBattle);
        if (requireBattle.boolValue)
        {
            EditorGUI.indentLevel = 1;
            CustomBoolField("hasCompletedBattle", hasCompletedBattle);
        }

        CustomBoolField("hasCompletedConversation", hasCompletedConversation);

        EditorGUI.indentLevel = 0;
        EditorGUILayout.PropertyField(allDailyTasksForLevel);

        serializedObject.ApplyModifiedProperties();
    }
    public void CustomFloatField(string fieldName, SerializedProperty prop)
    {
        prop.floatValue = EditorGUILayout.FloatField(fieldName, prop.floatValue);
    }
    public void CustomIntField(string fieldName, SerializedProperty prop)
    {
        prop.intValue = EditorGUILayout.IntField(fieldName, prop.intValue);
    }
    public void CustomBoolField(string fieldName, SerializedProperty prop)
    {
        prop.boolValue = EditorGUILayout.Toggle(fieldName, prop.boolValue);
    }

}


