using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DailySaveDataManager : MonoBehaviour
{
    public static event Action OnDailyMissionCompleted;

    public const string DailyTaskFileKey = "DailyTaskAllSaveData";

    public List<LevelData> allLevelData;
    public List<DailyTaskSaveData> allSaveData = new List<DailyTaskSaveData>();

    private void OnEnable()
    {
        LessonDisplayTextLogic.OnGuruLessonFinished += LessonDisplayTextLogic_OnGuruLessonFinished;
        Screen_ANTBattle.OnCombatFinished += OnFightFinishEvent;
    }


    private void OnDisable()
    {
        LessonDisplayTextLogic.OnGuruLessonFinished -= LessonDisplayTextLogic_OnGuruLessonFinished;
        Screen_ANTBattle.OnCombatFinished -= OnFightFinishEvent;
        
    }

    public void SaveAllLevelData()
    {
        for (int i = 0; i < allLevelData.Count; i++)
        {
            allLevelData[i].SaveLevelData();
        }
    }

    public void LoadAllLevelData()
    {
        for (int i = 0; i < allLevelData.Count; i++)
        {
            allLevelData[i].LoadLevelData();
        }
    }

    public void GenerateData()
    {
        for (int i = 0; i < allLevelData.Count; i++)
        {
            LevelData levelData = allLevelData[i];

            for (int j = 0; j < levelData.allDailyTasksForLevel.Count; j++)
            {
                DailyTask dailyTask = levelData.allDailyTasksForLevel[j];
                DailyTaskSaveData dailyTaskSave = new DailyTaskSaveData();
                dailyTaskSave.taskKey = $"{i + 1}_{dailyTask.dailyTaskKey}";
                dailyTaskSave.isCompleted = false;
                dailyTaskSave.levelIndex = i + 1;
                dailyTaskSave.experience = 0;
                dailyTaskSave.taskDescription = dailyTask.taskDescription;
                dailyTaskSave.taskHeaderName = dailyTask.taskHeaderName;

                SaveDailyTask(dailyTaskSave);
            }
        }
        
    }

    public void SaveDailyTask(DailyTaskSaveData taskData, bool shouldUpdate = false)
    {
        ES3.Save($"{taskData.taskKey}", taskData, DailyTaskFileKey);
        if(shouldUpdate)UpdateAllDailyData();
    }

    public bool DoesTaskDataFileExist() => ES3.FileExists(DailyTaskFileKey);

    public void LoadAllData()
    {
        LoadAllLevelData();
        allSaveData.Clear();
        
        string[] allKeys = ES3.GetKeys(DailyTaskFileKey);
        List<DailyTaskSaveData> saveDataList = new List<DailyTaskSaveData>();
        for (int i = 0; i < allKeys.Length; i++)
        {
            DailyTaskSaveData saveData = (DailyTaskSaveData)ES3.Load(allKeys[i], DailyTaskFileKey);
            if (saveData != null) saveDataList.Add(saveData);
        }
        allSaveData = saveDataList.OrderBy(d => d.levelIndex).ToList();

    }

    public void RestartAllLevels()
    {
        for (int i = 0; i < allLevelData.Count; i++)
        {
            allLevelData[i].ClearLevelData();
        }
    }

    public void UpdateAllDailyData()
    {
        LoadAllData();
        OnDailyMissionCompleted?.Invoke();
        //Next you need to update the rest by an event that save data has reloded
    }

    public void SaveAllData()
    {

    }

    public LevelData GetLevelData()
    {
        return allLevelData[SaveData.PlayerLevel - 1];
    }

    public LevelData GetLevelDataByIndex(int levelIndex)
    {
        LevelData levelToReturn = allLevelData.SingleOrDefault(l => l.levelNumber == levelIndex);
        return levelToReturn;
    }

    private void LessonDisplayTextLogic_OnGuruLessonFinished(int guruLesson)
    {
        LevelData data = allLevelData.SingleOrDefault(d => d.levelNumber == guruLesson);
        data.SetCompletedGuru();
    }
    private void OnFightFinishEvent(int battleIndex)
    {
        LevelData data = allLevelData.SingleOrDefault(d => d.levelNumber == battleIndex);
        data.SetCompletedCombat();
    }

}

[System.Serializable]
public class DailyTaskSaveData
{
    public bool isCompleted;
    public int levelIndex;
    public string taskKey;
    public string taskHeaderName;
    public string taskDescription;
    public float experience;

    public void UpdateSaveData()
    {
        ES3.Save(taskKey, this, DailySaveDataManager.DailyTaskFileKey);
    }
}
