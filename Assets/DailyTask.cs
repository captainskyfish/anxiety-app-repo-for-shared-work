﻿using Databox;
using System.Collections;
using UnityEngine;



public class DailyTask : ScriptableObject
{
    public int dailyTaskLevelRequirement;
    public string dailyTaskKey;
    public string taskDescription;
    public string taskHeaderName;

    public DailyTaskSaveData GetSaveData(int levelIndex)
    {
        return (DailyTaskSaveData)ES3.Load($"{levelIndex}_{dailyTaskKey}", DailySaveDataManager.DailyTaskFileKey);
    }
}
