using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenLogic : MonoBehaviour
{
    protected ScreenManager screenManager;
    public ScreenManager.AllScreens screen;
    public ScreenManager.Orientation orientation;

    public void SetupScreen(ScreenManager screenManager)
    {
        this.screenManager ??= screenManager;
        Setup();
    }

    public virtual void Setup()
    {

    }
}
