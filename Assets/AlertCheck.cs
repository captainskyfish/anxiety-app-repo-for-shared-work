using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AlertCheck : MonoBehaviour
{
    public ScreenManager.AllScreens screen;
    public GameObject exlamationMark;
    public void OnEnable()
    {
        CheckForAlert();
        EventManager.OnPlayerLevelUp += EventManager_OnPlayerLevelUp;
    }


    private void OnDisable()
    {
        EventManager.OnPlayerLevelUp -= EventManager_OnPlayerLevelUp;
    }

    public void CheckForAlert()
    {
        LevelData currentLevel = FindObjectOfType<DailySaveDataManager>().GetLevelData();

        bool hasTasksToDo = false;

        switch (screen)
        {
            case ScreenManager.AllScreens.Guru: hasTasksToDo = (currentLevel.requireGuru) ? (currentLevel.hasCompletedGuru) ? false : true : false; break;
            case ScreenManager.AllScreens.HallOfHeroes: hasTasksToDo = (currentLevel.requireDailyTasks) ? (currentLevel.HasFinishedDailyTasks()) ? false : true : false; break;
            case ScreenManager.AllScreens.ANTInstitute: hasTasksToDo = (currentLevel.requireBattle) ? (currentLevel.hasCompletedBattle)? false : true : false; break;
        }

        ToggleExclamationMark(hasTasksToDo);
    }

    public void ToggleExclamationMark(bool isActive)
    {
        exlamationMark.SetActive(isActive);
    }


    private void EventManager_OnPlayerLevelUp(object obj = null)
    {
        CheckForAlert();
    }
}
