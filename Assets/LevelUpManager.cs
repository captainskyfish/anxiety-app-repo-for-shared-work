using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class LevelUpManager : MonoBehaviour
{
    public static LevelUpManager instance;
    public static Action OnLevelUpTriggered;

    public GameObject levelUpDisplay;
    public TextMeshProUGUI levelUpValue;
    public RectTransform levelUpGraphic;

    public int levelToGoTo;
    [SerializeField] Transform conversationInstitute;

    public static int highlitedInstitute
    {
        get { return PlayerPrefs.GetInt(nameof(highlitedInstitute), 0); }
        set { PlayerPrefs.SetInt(nameof(highlitedInstitute), value); }
    }
    private bool ishighlightRunning = false;

    private void OnEnable()
    {
        Screen_MainMenu.OnReadyToLevelUp += Screen_MainMenu_OnReadyToLevelUp;
    }
    private void OnDisable()
    {
        Screen_MainMenu.OnReadyToLevelUp -= Screen_MainMenu_OnReadyToLevelUp;
    }

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        levelUpDisplay.SetActive(false);
    }

    private void Screen_MainMenu_OnReadyToLevelUp(int currentLevel)
    {
        //coroutine for level up display

        if (currentLevel == 1)
        {
            SaveData.CanAccessSignPedastal = true;
            OnLevelUpTriggered?.Invoke();
            return;
        }

        StartCoroutine(LevelUp());

    }
    public void LevelUpToLevelTwo()
    {
        StartCoroutine(LevelUp());
    }

    public IEnumerator LevelUp()
    {
        DailySaveDataManager gm = GameObject.Find("ApplicationManager").GetComponent<DailySaveDataManager>();
        if (SaveData.PlayerLevel < gm.allLevelData.Count)
        {
            yield return DisplayNewLevel();
            SaveData.PlayerLevel++;
        }
        OnLevelUpTriggered?.Invoke();

    }


    public IEnumerator DisplayNewLevel()
    {
        StartCoroutine(HighlightingConversationInst(3));
        levelUpGraphic.DOScale(0, 0);
        levelUpDisplay.SetActive(true);
        levelUpValue.text = "" + (SaveData.PlayerLevel + 1);
        levelUpGraphic.DOScale(1, 1.5f);
        yield return new WaitForSeconds(4);
        levelUpDisplay.SetActive(false);
    }
    
    public IEnumerator HighlightingConversationInst(float time)
    {
        highlitedInstitute = 1;
        yield return new WaitForSeconds(time);
        conversationInstitute.GetChild(0).GetChild(0).GetComponent<Image>().DOFade(1f, 0.1f);
    }
    public void WithoutHighlightedConversationInst()
    {
        highlitedInstitute = 0;
        conversationInstitute.GetChild(0).GetChild(0).GetComponent<Image>().DOFade(0.0f, 0.01f);
    }

    [ContextMenu("Level Up")]
    public void ForceLevelUp()
    {
        StartCoroutine(LevelUp());
    }

    [ContextMenu("Go To Custom Level")]
    public void GoToLevel()
    {
        SaveData.PlayerLevel = levelToGoTo;
        OnLevelUpTriggered?.Invoke();
    }

}
