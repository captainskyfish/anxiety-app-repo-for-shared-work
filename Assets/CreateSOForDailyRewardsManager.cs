﻿using Databox;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
#if USING_EDITOR
using UnityEditor;
[CreateAssetMenu(fileName = "DailyRewardsManager", menuName = "Create Daily Rewards Manager")]
public class CreateSOForDailyRewardsManager : MonoBehaviour
{

    public DataboxObject database;
    public const string AllDailyTasksKey = "AllDailyTasks";
    public const string LevelRequirementKey = "LevelRequirement";
    public const string DailyTaskKey = "DailyTaskKey";
    public const string DailyTaskDescriptionKey = "DailyTaskDescription";
    public const string DailyTaskNameKey = "DailyTaskName";

    public void Start()
    {
        if (!database.databaseLoaded) LoadDatabase();

        if (database.databaseLoaded) LoadForEachDatabase();
    }
    public void LoadDatabase()
    {
        database.LoadDatabase();
    }

    public void LoadForEachDatabase()
    {
        var data = database.GetEntriesFromTable("AllDailyTasks");

        foreach (var entry in data.Keys)
        {
            DailyTask task = CreateDailyTaskHolder();

            PopulateAsset(task, entry);

            CreateAsset(task, $"{task.dailyTaskKey}_{task.dailyTaskLevelRequirement}");
        }
        SaveAllAssets();
    }

    public DailyTask CreateDailyTaskHolder()
    {
        return ScriptableObject.CreateInstance<DailyTask>();
    }

    public void CreateAsset(DailyTask task, string name)
    {
        AssetDatabase.CreateAsset(task, $"Assets/DailyTasks/{name}.asset");
    }

    public void PopulateAsset(DailyTask task, string entry)
    {
        task.dailyTaskLevelRequirement = DataboxLib.GetIntData(database, AllDailyTasksKey, entry, LevelRequirementKey);
        task.dailyTaskKey = DataboxLib.GetStringData(database, AllDailyTasksKey, entry, DailyTaskKey);
        task.taskDescription = DataboxLib.GetStringData(database, AllDailyTasksKey, entry, DailyTaskDescriptionKey);
        task.taskHeaderName = DataboxLib.GetStringData(database, AllDailyTasksKey, entry, DailyTaskNameKey);
    }

    public void SaveAllAssets()
    {
        AssetDatabase.SaveAssets();
    }
}
#endif
