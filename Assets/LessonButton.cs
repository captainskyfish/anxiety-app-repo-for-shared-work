using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LessonButton : MonoBehaviour
{
    public int levelRequirement;
    public Button lessonButton;
    public Lesson lesson;

    public TextMeshProUGUI lessonNameText;

    private void Awake()
    {
        lessonButton = GetComponent<Button>();
    }



    public void Setup(Lesson lesson)
    {
        this.lesson = lesson;
        levelRequirement = lesson.levelRequirement;
        
        SetupText(lesson);
    }

    public void SetupText(Lesson lesson)
    {
        lessonNameText.text = $"Lesson {lesson.levelRequirement}: {lesson.lessonName}";
    }

    public void SetButtonGraphic(Color col)
    {
        GetComponent<Image>().color = col;
    }
}
