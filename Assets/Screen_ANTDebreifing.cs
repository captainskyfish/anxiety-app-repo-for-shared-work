using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screen_ANTDebreifing : ScreenLogic
{
    public Fight fight;
    public ContinousTextBox textBox;
    public GameObject finishButton;
    public override void Setup()
    {

        finishButton.SetActive(false);

        string seperator = "[!]";
        string[] allAdviceText = fight.debreifingText.Split(new string[] { seperator }, StringSplitOptions.None);
        textBox.StartTextBox(allAdviceText, OnFinishDebreifing);
    }

    public void OnFinishDebreifing()
    {
        finishButton.SetActive(true);
        print("finished all text");
    }
    

}
