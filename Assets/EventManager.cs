﻿using System;
using System.Collections;
using UnityEngine;

public static class EventManager
{

    public delegate void PlayerEvents(object obj = null);
    public static event PlayerEvents OnPlayerLevelUp;

    public static void TriggerLevelUp(int newLevel)
    {
        OnPlayerLevelUp?.Invoke(newLevel);
    }

    public delegate void AntBattleEvents();
    public static event AntBattleEvents OnFightStart;
    public static event AntBattleEvents OnMonsterFinishedTalking;
    public static event AntBattleEvents OnFinishFight;
    public static event AntBattleEvents OnCorrectAnswer;
    public static event AntBattleEvents OnWrongAnswer;

    public static void TriggerMonsterFinishTalking()
    {
        OnMonsterFinishedTalking?.Invoke();
    }
    public static void TriggerCorrectAnswer()
    {
        OnCorrectAnswer?.Invoke();
    }
    public static void TriggerWrongAnswer()
    {
        OnWrongAnswer?.Invoke();
    }
    public static void TriggerFinishFight()
    {
        OnFinishFight?.Invoke();
    }

    public static void TriggerFightStart()
    {
        Debug.Log("HElp?");
        OnFightStart?.Invoke();
    }
}