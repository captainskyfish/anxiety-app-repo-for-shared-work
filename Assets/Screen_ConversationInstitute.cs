﻿using Databox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Screen_ConversationInstitute : ScreenLogic
{
    const string ConversationInstitutreKey = "ConversationInstitute";
    const string LevelRequirementKey = "LevelRequirement";
    const string AdviceTextKey = "AdviceText";
    const string AdviceNameKey = "AdviceName";

    public static Action<bool> OnConversationStateChange;

    [Header("Guru Page Specifics")]
    public DailySaveDataManager levelSaveDataManager;
    ConversationSelectionLogic conversationLessonLogic;

    public DataboxObject conversationDatabase;
    public List<Conversation> allLessons = new List<Conversation>();
    public GameObject backButton;
    public ConversationSelectionLogic lessonSelectionLogic;
    public Color[] hasCompletedColours;

    private void OnEnable()
    {
        OnConversationStateChange += ToggleStateChange;
    }

    private void OnDisable()
    {
        OnConversationStateChange -= ToggleStateChange;
    }

    private void ToggleStateChange(bool isConversationActive)
    {
        backButton.SetActive(!isConversationActive);
    }
    public override void Setup()
    {
        levelSaveDataManager = FindObjectOfType<DailySaveDataManager>();

        if (!conversationDatabase.databaseLoaded) LoadDatabase();

        if (conversationDatabase.databaseLoaded) SetupAllLessons();

        conversationLessonLogic = FindObjectOfType<ConversationSelectionLogic>();
        conversationLessonLogic.Setup(allLessons);

        FinalHighlightInts();
    }
    public void FinalHighlightInts()
    {
        var butns = conversationLessonLogic.allSpawnedButtons.Find(e => e.isEnable && !e.isUnloked);
        if (butns)
        {
            LevelUpManager.highlitedInstitute = 1;
            Debug.Log("image true");
        }
        else
        {
            LevelUpManager.highlitedInstitute = 0;
            Debug.Log("image false");
        }
    }

    public void LoadDatabase()
    {
        conversationDatabase.LoadDatabase();
    }

    public void SetupAllLessons()
    {
        var data = conversationDatabase.GetEntriesFromTable(ConversationInstitutreKey);

        foreach (var entry in data.Keys.Reverse())
        {
            int levelReq = 0;
            string lessonText = string.Empty;
            string lessonHeader = string.Empty;

            foreach (var item in conversationDatabase.GetValuesFromEntry(ConversationInstitutreKey, entry))
            {
                levelReq = GetIntData(conversationDatabase, ConversationInstitutreKey, entry, LevelRequirementKey);
                lessonText = GetStringData(conversationDatabase, ConversationInstitutreKey, entry, AdviceTextKey);
                lessonHeader = GetStringData(conversationDatabase, ConversationInstitutreKey, entry, AdviceNameKey);
            }

            Conversation lesson = new Conversation(levelReq, lessonText, lessonHeader);
            allLessons.Add(lesson);
        }

    }


    #region DatabaseStuff
    public static int GetIntData(DataboxObject database, string tableID, string entryID, string valueID)
    {
        IntType _value;
        if (database.TryGetData<IntType>(tableID, entryID, valueID, out _value))
        {
            return _value.Value;
        }
        else
        {
            return 0;
        }
    }

    private static string GetStringData(DataboxObject database, string tableID, string entryID, string valueID)
    {
        StringType _StringType;
        if (database.TryGetData<StringType>(tableID, entryID, valueID, out _StringType))
        {
            return _StringType.Value;
        }
        else
        {
            return null;
        }
    }
    #endregion DatabaseStuff

}

[System.Serializable]
public class Conversation
{
    public int levelRequirement;
    public string[] allAdviceText;
    public string adviceName;
    public Conversation(int levelReq, string text, string adviceName = "No Name")
    {
        levelRequirement = levelReq;
        this.adviceName = adviceName;
        SetupText(text);
    }

    public void SetupText(string text)
    {
        string seperator = "[!]";
        allAdviceText = text.Split(new string[] { seperator }, StringSplitOptions.None);
    }
}
