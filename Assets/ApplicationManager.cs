﻿using System.Collections;
using UnityEngine;


public class ApplicationManager : MonoBehaviour
{

    public GameObject welcomePopup;

    public void Awake()
    {
        InitialiseApplication();
    }

    public void InitialiseApplication()
    {
        if (IsFirstTime())
            FirstTimeLogic();
        else
            FindObjectOfType<DailySaveDataManager>().LoadAllData();

    }

    public bool IsFirstTime()
    {
        return SaveData.IsFirstTime;
    }

    public void FirstTimeLogic()
    {
        SaveData.SetCurrentSpeed(Utility_TextScrolling.ScrollingSpeeds.Fast);
        SaveData.PlayerLevel = 1;
        SaveData.IsFirstTime = false;
        SaveData.HasSignedPedastal = false;
        SaveData.CanAccessSignPedastal = false;
        SaveData.HasEnteredBattleBefore = false;

        FindObjectOfType<DailySaveDataManager>().RestartAllLevels();
        FindObjectOfType<DailySaveDataManager>().SaveAllLevelData();
        FindObjectOfType<DailySaveDataManager>().GenerateData();
        FindObjectOfType<DailySaveDataManager>().LoadAllData();

        welcomePopup.SetActive(true);

        SaveData.SetCurrentSpeed(Utility_TextScrolling.ScrollingSpeeds.Normal);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
            SaveData.PlayerLevel++;
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
            SaveData.PlayerLevel--;
    }

    public void SetTextSpeed(int speedIndex)
    {
        SaveData.SetCurrentSpeed((Utility_TextScrolling.ScrollingSpeeds)speedIndex);
    }
}