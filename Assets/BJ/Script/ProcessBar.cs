using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class ProcessBar : MonoBehaviour
{
    public Image filledImage;
    public GameObject partical;

    float fillValue = 0;

    public static float LevelfillValue
    {
        get { return PlayerPrefs.GetFloat(nameof(LevelfillValue), 0); }
        set { PlayerPrefs.SetFloat(nameof(LevelfillValue), value); }
    }

    private void OnEnable()
    {
        filledImage.fillAmount = LevelfillValue;
        partical.SetActive(false);
        StartCoroutine(OnDelayToPlay());
    }
    IEnumerator OnDelayToPlay()
    {
        yield return new WaitForSeconds(2f);
        Fill();
    }
    private void Fill()
    {
        fillValue = LevelfillValue;
        partical.SetActive(true);
        DOTween.To(() => fillValue, x => fillValue = x, GetCurrentLevelFillValue(), 1f).OnUpdate(() => UpdateBar()).OnComplete(() => partical.SetActive(false));
    }
    private void UpdateBar()
    {
        filledImage.fillAmount = fillValue;
        var posX = Mathf.Lerp(-355, 355, fillValue);
        partical.transform.localPosition = new Vector2(posX, 0);
    }

    private float GetCurrentLevelFillValue()
    {
        // total levels / current level
        int currentLevel = (SaveData.PlayerLevel + 1);
        int totalLevel = FindObjectOfType<DailySaveDataManager>().allLevelData.Count;
        Debug.Log($"currentLevels{currentLevel}-----totalLevels{totalLevel}");
        float a = (float)((float)currentLevel / (float)totalLevel);
        LevelfillValue = a; // levels
        return LevelfillValue;
    }
}
