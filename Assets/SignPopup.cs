using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignPopup : MonoBehaviour
{
    public GameObject agreeButtonsHolder;
    public GameObject backButtonContainer;

    public void ToggleAgreeButtons(bool agreeButtonsOn)
    {
        agreeButtonsHolder.SetActive(agreeButtonsOn);
        backButtonContainer.SetActive(!agreeButtonsOn);
    }
}
